using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Y_MainSystems;

public class NPC : PoolObj
{
    [SerializeField] float changeDirTime;
    [SerializeField] float changeDirTimeRandom;
    [SerializeField] float speed;
    [SerializeField] BlockDetector blockDetector;

    [SerializeField] GameObject model;
    [SerializeField] Animator anim;

    Vector3 way;
    float nextChangeDirTime;

    bool isBackMoveStart;

    public override void initilaze()
    {
        base.initilaze();
        anim.SetBool("IsWalk", true);
    }

    void WanderAround()
    {
        //  Change dir check
        if (blockDetector.isTouch )
        {
            if (!isBackMoveStart)
            {
                way *= -1;
                isBackMoveStart = true;
            }
        }
        else
        {
            isBackMoveStart = false;
        }

        if (Time.time >= nextChangeDirTime)
        {
            way = new Vector3(Random.Range(-100, 100), 0, Random.Range(-100, 100)).normalized;
            nextChangeDirTime = Time.time + changeDirTime + Random.Range(0, changeDirTimeRandom);
        }

        //  Move
        model.transform.forward = way;
        transform.Translate(way * speed * Time.deltaTime);
        anim.SetBool("IsWalk", true);
    }

    private void Update()
    {
        if (inUse && GameManeger.state == GameState.InGame)
        {
            WanderAround();
        }
        else
        {
            anim.SetBool("IsWalk", false);
        }
    }

}
