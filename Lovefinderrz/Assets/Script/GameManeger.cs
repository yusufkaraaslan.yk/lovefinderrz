using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Y_MainSystems;

public class GameManeger : MonoBehaviour
{
    public static GameState state;
    [SerializeField] Map map;
    [SerializeField] Player player;
    [SerializeField] PlayerPhone phone;
    [SerializeField] GameData gameData;

    UIManeger ui;

    public void OpenMenu()
    {
        state = GameState.Menu;

        ui.CloseUI("App");
        ui.CloseUI("Result");
        ui.CloseUI("Loading");
        ui.CloseUI("NoSignal");

        ui.OpenUI("Menu");

        phone.BigMode();
        SoundManeger.GetInstance().PlayMusic("Menu");
    }

    public void StartGame()
    {
        gameData.ResetGameData();
        gameData.StartGameTime();
        player.StartPlayer();

        state = GameState.InGame;

        phone.SmallMode();
        SoundManeger.GetInstance().PlayMusic("Crowds");
    }

    public void EndGame()
    {
        state = GameState.Result;

        ui.CloseUI("App");
        ui.CloseUI("NoSignal");
        ui.OpenUI("Result");

        phone.BigMode();
        SoundManeger.GetInstance().StopMusic();
    }

    private void Start()
    {
        ui = UIManeger.GetInstance();

        SoundManeger.GetInstance().MusicOn();
        SoundManeger.GetInstance().SoundOn();

        SoundManeger.GetInstance().SetMusicVolume(0.15f);
        SoundManeger.GetInstance().SetSoundValume(0.4f);

        SoundManeger.GetInstance().PlayMusic("Menu");
    }

    private void Update()
    {
        if (state == GameState.InGame)
        {
            if (GameData.remainTime <= 0)
            {
                EndGame();
            }
        }

    }

}

public enum GameState
{
    Menu, InGame, Result
}