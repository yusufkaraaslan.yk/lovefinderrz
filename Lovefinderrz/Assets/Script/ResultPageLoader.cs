using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResultPageLoader : MonoBehaviour
{
    [SerializeField] GameData gameData;

    [SerializeField] EndingIcon fameEnd, loveEnd, fameAndLoveEnd, TheOneEnd;

    bool isFameUnlucked, isLoveUnlucked, isFameAndLoveUnlocked;

    void LoadResult()
    {
        float famePercent = gameData.GetStat(PlayerStatType.Fame).val / gameData.maxStatVal;
        float lovePercent = gameData.GetStat(PlayerStatType.Love).val / gameData.maxStatVal;

        bool isFame = famePercent >= 0.75f;
        bool isLove = lovePercent >= 0.75f;

        //  Check Current Result
        if (isFame && isLove)
        {
            isFameAndLoveUnlocked = true;
        }
        else if (isFame)
        {
            isFameUnlucked = true;
        }
        else if (isLove)
        {
            isLoveUnlucked = true;
        }

        //  Open Unlocked Ending
        if (isFameAndLoveUnlocked)
        {
            fameAndLoveEnd.OpenIcon();
        }

        if (isFameUnlucked)
        {
            fameEnd.OpenIcon();
        }

        if (isLoveUnlucked)
        {
            loveEnd.OpenIcon();
        }

        if (GameData.IsFindTheOne)
        {
            TheOneEnd.OpenIcon();
        }

    }

    private void OnEnable()
    {
        LoadResult();
    }

}
