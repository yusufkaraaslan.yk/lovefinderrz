using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheOneEnding : MonoBehaviour
{
    [SerializeField] GameManeger gm;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            GameData.IsFindTheOne = true;
            gm.EndGame();
        }
    }

}
