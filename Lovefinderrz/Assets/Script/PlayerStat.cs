using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Player Stat", menuName = "Player Stat", order = 51)]
public class PlayerStat : ScriptableObject
{
    [SerializeField] public PlayerStatType type;
    [HideInInspector] public float val;
    [SerializeField] public Sprite icon;
    [SerializeField] float DefultVal = 35;

    public void ResetVal()
    {
        val = DefultVal;
    }

}

public enum PlayerStatType
{
    Fame, Love
}
