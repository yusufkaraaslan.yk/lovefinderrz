using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockDetector : MonoBehaviour
{
    public bool isTouch;

    private void OnTriggerStay(Collider other)
    {
        if (other.tag != "City")
        {
            isTouch = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag != "City")
        {
            isTouch = false;
        }
    }

}
