using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMap : MonoBehaviour
{
    [SerializeField] Transform arrow;
    [SerializeField] GameObject player;
    [SerializeField] Map map;

    [SerializeField] float minDistance;

    void UpdateMiniMap()
    {
        NPC tmp = map.GetTarget();

        if (tmp != null)
        {
            Transform tar = tmp.transform;

            Vector3 way = player.transform.position - tar.position;

            way = new Vector3(way.x, way.z, 0).normalized;

            arrow.up = way;

            if (Mathf.Abs(Vector3.Distance(player.transform.position,tar.position)) < minDistance)
            {
                player.GetComponent<Player>().InterectToTarget();
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManeger.state == GameState.InGame)
        {
            UpdateMiniMap();
        }
    }
}
