using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Y_MainSystems;

public class UIButtonProxy : MonoBehaviour
{
    [SerializeField] GameManeger gm;
    [SerializeField] Animator phoneAnim;
    [SerializeField] GameData gameData;
    [SerializeField] Map map;

    public void StartGame()
    {
        List<VisualEventData> events = new List<VisualEventData>();
        VisualEventData eventData = new VisualEventData();
        events.Add(eventData);

        eventData.SetAnimData(vePROFILES.Instance.GetAnimData("OpenApp", 0));

        eventData.GetAnim().animOrders[0].anim = phoneAnim;

        eventData.SetFunctionData(new FunctionData());

        eventData.GetFunctions().AddFunction(new Function_type2());
        eventData.GetFunctions().functions[0].workPhase = EventWorkPhase.PreActivate;
        ((Function_type2)eventData.GetFunctions().functions[0]).input = "Loading";
        ((Function_type2)eventData.GetFunctions().functions[0]).action = OpenPanel;

        eventData.GetFunctions().AddFunction(new BasicFunction());
        eventData.GetFunctions().functions[1].workPhase = EventWorkPhase.PreActivate;
        eventData.GetFunctions().functions[1].preDelay = 0.8f;
        ((BasicFunction)eventData.GetFunctions().functions[1]).action = gm.StartGame;

        eventData.GetFunctions().AddFunction(new Function_type2());
        eventData.GetFunctions().functions[2].workPhase = EventWorkPhase.PreActivate;
        eventData.GetFunctions().functions[2].preDelay = 0.8f;
        ((Function_type2)eventData.GetFunctions().functions[2]).input = "App";
        ((Function_type2)eventData.GetFunctions().functions[2]).action = OpenPanel;

        eventData.GetFunctions().AddFunction(new Function_type2());
        eventData.GetFunctions().functions[3].workPhase = EventWorkPhase.PreActivate;
        eventData.GetFunctions().functions[3].preDelay = 0.8f;
        ((Function_type2)eventData.GetFunctions().functions[3]).input = "Loading";
        ((Function_type2)eventData.GetFunctions().functions[3]).action = ClosePanel;

        eventData.GetFunctions().AddFunction(new BasicFunction());
        eventData.GetFunctions().functions[4].workPhase = EventWorkPhase.PreActivate;
        ((BasicFunction)eventData.GetFunctions().functions[4]).action = PlayTouchSound;

        eventData.GetFunctions().AddFunction(new BasicFunction());
        eventData.GetFunctions().functions[5].workPhase = EventWorkPhase.PreActivate;
        eventData.GetFunctions().functions[5].preDelay = 0.8f;
        ((BasicFunction)eventData.GetFunctions().functions[5]).action = PlayAppOpenSound;

        VisualEventController.Instance().AddVisualEvent(events);
    }

    void PlayTouchSound()
    {
        SoundManeger.GetInstance().PlaySound("Touch");
    }

    void PlayAppOpenSound()
    {
        SoundManeger.GetInstance().PlaySound("AppOpen");
    }

    void OpenPanel(string panelName)
    {
        UIManeger.GetInstance().OpenUI(panelName);
    }

    void ClosePanel(string panelName)
    {
        UIManeger.GetInstance().CloseUI(panelName);
    }

    public void AddFame()
    {
        gameData.GainStats(PlayerStatType.Fame);
        Time.timeScale = 1;
        UIManeger.GetInstance().CloseUI("Option");
        SoundManeger.GetInstance().PlaySound("Touch");

        map.SetNewTarget();
    }

    public void AddLove()
    {
        gameData.GainStats(PlayerStatType.Love);
        Time.timeScale = 1;
        UIManeger.GetInstance().CloseUI("Option");
        SoundManeger.GetInstance().PlaySound("Touch");

        map.SetNewTarget();
    }

    public void BackToMenu()
    {
        gm.OpenMenu();
        SoundManeger.GetInstance().PlaySound("AppClose");
    }

}
