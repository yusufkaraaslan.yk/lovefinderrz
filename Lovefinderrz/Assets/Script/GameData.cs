using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData : MonoBehaviour
{
    public static float remainTime;
    public static float gameTime;

    public static bool IsFindTheOne;

    public PlayerStat[] stats;

    [SerializeField] float decreasePercent;
    [SerializeField] float gainPercent;

    [SerializeField] float gameTimeBase;
    [SerializeField] float gameTimeRandom;

    public float maxStatVal = 100;

    public void StartGameTime()
    {
        remainTime = gameTimeBase + Random.Range(0, gameTimeRandom);
        gameTime = remainTime;
    }

    public void GainStats(PlayerStatType type)
    {
        for (int i = 0; i < stats.Length; i++)
        {
            if (stats[i].type == type)
            {

                if (stats[i].val < maxStatVal)
                {
                    stats[i].val += maxStatVal * gainPercent;
                }
                else
                {
                    stats[i].val = maxStatVal;
                }

                break;
            }
        }
    }

    public void ResetGameData()
    {
        for (int i = 0; i < stats.Length; i++)
        {
            stats[i].ResetVal();
        }
    }

    public PlayerStat GetStat(PlayerStatType type)
    {
        for (int i = 0; i < stats.Length; i++)
        {
            if (stats[i].type == type)
            {
                return stats[i];
            }
        }

        return null;
    }

    public void LoseStat()
    {
        for (int i = 0; i < stats.Length; i++)
        {
            if (stats[i].val > 0)
            {
                stats[i].val -= maxStatVal * decreasePercent * Time.deltaTime;
            }
            else
            {
                stats[i].val = 0;
            }

        }
    }

    private void Update()
    {
        if (GameManeger.state == GameState.InGame)
        {
            remainTime -= Time.deltaTime;
            LoseStat();
        }
    }

}
