using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerPhone : MonoBehaviour
{
    [SerializeField] GameData gameData;
    [SerializeField] Slider fameBar, loveBar, timeBar;
    [SerializeField] Animator anim;

    public void BigMode()
    {
        anim.SetBool("isBigModeOn", true);
    }

    public void SmallMode()
    {
        anim.SetBool("isBigModeOn", false);
    }

    public void UpdatePhone()
    {
        fameBar.value = gameData.GetStat(PlayerStatType.Fame).val / gameData.maxStatVal;
        loveBar.value = gameData.GetStat(PlayerStatType.Love).val / gameData.maxStatVal;
        timeBar.value = (GameData.remainTime) / GameData.gameTime;
    }

    private void Update()
    {
        if (GameManeger.state == GameState.InGame)
        {
            UpdatePhone();
        }
    }

}
