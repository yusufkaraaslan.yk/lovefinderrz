using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Y_MainSystems;

public class Map : MonoBehaviour
{
    [SerializeField] GameObject[] spawnPoints;
    List<NPC> npcs;
    int targetNPC;
    PoolSystem pool;

    public void Init()
    {
        pool = PoolSystem.GetInstance();
    }

    public void LoadMap()
    {
        npcs = new List<NPC>();

        foreach (GameObject pos in spawnPoints)
        {
            NPC tmp = pool.GetObj("NPC", pos, false).GetComponent<NPC>();
            npcs.Add(tmp);
        }

        SetNewTarget();
    }

    public void SetNewTarget()
    {
        targetNPC = Random.Range(0, npcs.Count);
    }

    public NPC GetTarget()
    {
        if (targetNPC < npcs.Count && npcs.Count > 0)
        {
            return npcs[targetNPC];
        }

        return null;
    }

    public void ClearLevel()
    {
        for (int i = 0; i < npcs.Count; i++)
        {
            npcs[i].DespawnObj();
        }
    }

}
