using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class EndingIcon : MonoBehaviour
{
    [SerializeField] Image Icon;
    [SerializeField] Animator anim;
    [SerializeField] Sprite iconSprite;
    bool isOpened;


    public void OpenIcon()
    {
        if (isOpened)
        {
            anim.Play("Idle");
        }
        else
        {
            Icon.color = Color.white;
            Icon.sprite = iconSprite;
            isOpened = true;
            anim.Play("JustOpen");
        }
    }

}
