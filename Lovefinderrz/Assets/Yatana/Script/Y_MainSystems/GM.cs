﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Y_GP;

namespace Y_MainSystems
{
    public class GM : MonoBehaviour
    {
        [SerializeField]
        Player player;
        [SerializeField]
        PoolSystemAdaptor pool;
        [SerializeField]
        UIManegerAdaptor ui;
        [SerializeField]
        CamSystemAdaptor cam;

        [SerializeField]
        Map map;


        public void initilazeGame()
        {
            if (player != null)
            {
                player.Initilaze();
            }

            if (pool != null)
            {
                pool.Initialize();
            }

            if (ui != null)
            {
                ui.initilaze();
            }

            if (cam != null)
            {
                cam.initilaze();
            }

            if (map != null)
            {
                map.Init();
                map.LoadMap();
            }

        }

        private void Awake()
        {
            initilazeGame();
        }

    }
}
