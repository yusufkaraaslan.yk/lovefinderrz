﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Y_MainSystems;

public class RainbowRay : PoolObj
{
    [SerializeField] TwoPointRay lineRenderer, fxLineRenderer;
    [SerializeField] Transform startTransform, endTransform, hitFxTransform;
    [SerializeField] Transform movedStartTransform, movedEndTransform;

    [SerializeField] float lerp = 4f;
    [SerializeField] float reachDelay = 0.33f;
    [SerializeField] Vector3 offset = new Vector3(0, 0, -0.5f);

    Vector3 startPos;
    Vector3 endPos;
    Vector3 currentPos;

    bool isActive;
    bool isReachedEnd;
    bool isReachedStart;
    float timer;

    public override bool SpawnObj(Transform pos, bool useRotation, bool useScale, bool setParent = false, GameObject p = null)
    {
        return base.SpawnObj(pos, useRotation, useScale, setParent, p);
    }

    public override void initilaze()
    {
        gameObject.SetActive(false);
        base.initilaze();
    }

    public void SetRayStart(Vector3 worldPos)
    {
        startPos = worldPos + offset;
        startTransform.position = startPos;
        movedStartTransform.position = startPos;
    }

    public void SetRayEnd(Vector3 worldPos)
    {
        endPos = worldPos + offset;
        endTransform.position = endPos;
    }

    public void SetActive()
    {
        currentPos = startPos;

        lineRenderer.SetActive(true);
        fxLineRenderer.SetActive(true);

        gameObject.SetActive(true);
        hitFxTransform.gameObject.SetActive(false);

        movedEndTransform.position = currentPos;

        timer = 0;

        isReachedStart = false;
        isReachedEnd = false;
        isActive = true;
    }

    public void Dismiss()
    {
        isActive = false;
        DespawnObj();
    }

    public override void DespawnObj(bool restartObj = true)
    {
        base.DespawnObj(restartObj);

        lineRenderer.SetActive(false);
        fxLineRenderer.SetActive(false);

        gameObject.SetActive(false);
    }

    private void Update()
    {
        if (isActive)
        {
            currentPos = Vector3.MoveTowards(currentPos, endPos, lerp * Time.deltaTime);
            movedEndTransform.position = currentPos;

            if (!isReachedEnd)
            {
                if (Vector3.Distance(currentPos, endPos) < 0.1)
                {
                    hitFxTransform.position = endPos;
                    hitFxTransform.gameObject.SetActive(true);
                    isReachedEnd = true;
                }
            }
            else if (!isReachedStart)
            {
                timer += Time.deltaTime;
                if (timer > reachDelay)
                {
                    movedStartTransform.position = Vector3.MoveTowards(movedStartTransform.position, endPos, lerp * Time.deltaTime);
                    if (Vector3.Distance(movedStartTransform.position, endPos) < 0.1)
                    {
                        lineRenderer.SetActive(false);
                        fxLineRenderer.SetActive(false);
                        isReachedStart = true;
                    }
                }
            }
        }
    }
}
