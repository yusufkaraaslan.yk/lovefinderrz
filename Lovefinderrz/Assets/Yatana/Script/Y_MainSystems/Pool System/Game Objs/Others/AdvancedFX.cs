﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdvancedFX : MonoBehaviour
{
    [SerializeField] ParticleSystem particle;

    [Header("Rate Over Time")]
    [SerializeField] float activeRateOverTime = 1;
    [SerializeField] float passiveRateOverTime = 0;

    [Header("Rate Over Distance")]
    [SerializeField] float activeRateOverDistance = 0;
    [SerializeField] float passiveRateOverDistance = 0;

    ParticleSystem.EmissionModule emission;
    ParticleSystemRenderer particleRenderer;

    public float LenghtScale
    {
        set
        {
            particleRenderer.lengthScale = value;
        }
        get
        {
            return particleRenderer.lengthScale;
        }
    }

    public void Initialize()
    {
        emission = particle.emission;
        particleRenderer = particle.GetComponent<ParticleSystemRenderer>();
    }

    public void Activate()
    {
        emission.rateOverDistance = activeRateOverDistance;
        emission.rateOverTime = activeRateOverTime;
        particle.Clear();
        particle.Play();
    }

    public void Passive()
    {
        emission.rateOverDistance = passiveRateOverDistance;
        emission.rateOverTime = passiveRateOverTime;

        particle.Stop();
        particle.Clear();
    }
}
