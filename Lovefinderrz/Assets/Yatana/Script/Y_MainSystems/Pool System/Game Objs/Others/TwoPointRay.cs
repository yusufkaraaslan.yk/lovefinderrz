﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwoPointRay : MonoBehaviour
{
    [SerializeField] private LineRenderer lineRenderer;
    public Transform startPoint;
    public Transform endPoint;
    //private readonly int pointsCount = 2;
    //private Vector3[] points;

    //private readonly int pointIndexA = 0;
    //private readonly int pointIndexB = 1;
    //private readonly int pointIndexC = 2;
    //private readonly int pointIndexD = 3;
    //private readonly int pointIndexE = 4;

    //private float timer;
    //private float timerTimeOut = 0.05f;

    //private void Start()
    //{
    //    //points = new Vector3[pointsCount];
    //    //lineRenderer.positionCount = pointsCount;
    //}

    public void SetActive(bool isActive)
    {
        //if (!isActive)
        //{
            lineRenderer.SetPosition(0, Vector3.zero);
            lineRenderer.SetPosition(1, Vector3.zero);
        //}

        gameObject.SetActive(isActive);
    }

    public void SetWidth(float w)
    {
        lineRenderer.startWidth = w;
        lineRenderer.endWidth = w;
    }

    private void Update()
    {
        lineRenderer.SetPosition(0, startPoint.position);
        lineRenderer.SetPosition(1, endPoint.position);
    }

    //private void CalculatePoints()
    //{
    //    timer += Time.deltaTime;

    //    if (timer > timerTimeOut)
    //    {
    //        timer = 0;

    //        //points[pointIndexA] = startPoint.position;
    //        //points[pointIndexE] = endPoint.position;
    //        //points[pointIndexC] = GetCenter(points[pointIndexA], points[pointIndexE]);
    //        //points[pointIndexB] = GetCenter(points[pointIndexA], points[pointIndexC]);
    //        //points[pointIndexD] = GetCenter(points[pointIndexC], points[pointIndexE]);

    //        lineRenderer.SetPositions(points);
    //    }
    //}

    //private Vector3 GetCenter(Vector3 a, Vector3 b)
    //{
    //    return (a + b) / 2;
    //}
}

