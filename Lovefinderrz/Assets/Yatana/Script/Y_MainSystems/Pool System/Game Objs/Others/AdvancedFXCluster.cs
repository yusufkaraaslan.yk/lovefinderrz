﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdvancedFXCluster : MonoBehaviour
{
    [SerializeField] List<AdvancedFX> particles;

    public void Initialize()
    {
        foreach (var fx in particles)
        {
            fx.Initialize();
        }
    }

    public void Activate()
    {
        foreach (var fx in particles)
        {
            fx.Activate();
        }
    }

    public void Passive()
    {
        foreach (var fx in particles)
        {
            fx.Passive();
        }
    }
}
