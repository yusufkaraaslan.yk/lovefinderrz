﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Y_Helper;

namespace Y_MainSystems
{
    public class PPVisual : PoolObj
    {
        [SerializeField]
        protected Animator anim;

        [SerializeField]
        string[] extraRestartTrigger;

        [SerializeField]
        AnimBoolData[] extraRestartBool;

        [SerializeField]
        AnimIntData[] extraRestartInt;



        public override void initilaze()
        {
            base.initilaze();
        }

        public override bool SpawnObj(Vector3 pos,
            bool useRotation, Quaternion rot, bool useScale, Vector3 scale, bool setParent = false, GameObject p = null)
        {
            bool res = base.SpawnObj(pos, useRotation, rot, useScale, scale, setParent, p);

            if (res == true)
            {
                anim.SetBool("InUse", true);
                anim.SetTrigger("Spawn");
            }

            return res;
        }

        public void InitilazePuzzlePieceVisual(List<string> extraTrigger, List<AnimBoolData> extraBool, List<AnimIntData> extraInt)
        {
            if (extraTrigger != null)
            {
                foreach (string x in extraTrigger)
                {
                    anim.SetTrigger(x);
                }
            }

            if (extraBool != null)
            {
                foreach (AnimBoolData x in extraBool)
                {
                    anim.SetBool(x.fieldName, x.val);
                }
            }

            if (extraInt != null)
            {
                foreach (AnimIntData x in extraInt)
                {
                    anim.SetInteger(x.fieldName, x.val);
                }
            }
        }

        public void DespawnObj()
        {
            DespawnObj(true);
        }

        public override void DespawnObj(bool restartObj = true)
        {
            RestartParent();

            RestartAnimData();

            base.DespawnObj(restartObj);
        }

        public Animator GetAnimator()
        {
            return anim;
        }

        public void TriggerAnim(string trigger)
        {
            anim.SetTrigger(trigger);
        }

        public void ResetTrigger(string trigger)
        {
            anim.ResetTrigger(trigger);
        }

        public void SetAnimInt(string t, int val)
        {
            anim.SetInteger(t, val);
        }

        public void SetAnimBool(string t, bool val)
        {
            anim.SetBool(t, val);
        }

        void RestartAnimData()
        {
            foreach (AnimatorControllerParameter item in anim.parameters)
            {
                switch (item.name)
                {
                    case "Spawn":
                        anim.ResetTrigger("Spawn");
                        break;
                    case "Despawn":
                        anim.SetTrigger("Despawn");
                        break;
                    case "Selected":
                        anim.SetBool("Selected", false);
                        break;
                    case "InUse":
                        anim.SetBool("InUse", false);
                        break;
                    case "Shake_Once":
                        anim.ResetTrigger("Shake_Once");
                        break;
                    case "Shake":
                        anim.SetBool("Shake", false);
                        break;
                }
            }

            if (extraRestartTrigger != null)
            {
                foreach (string x in extraRestartTrigger)
                {
                    anim.ResetTrigger(x);
                }
            }

            if (extraRestartBool != null)
            {
                foreach (AnimBoolData x in extraRestartBool)
                {
                    anim.SetBool(x.fieldName, x.val);
                }
            }

            if (extraRestartInt != null)
            {
                foreach (AnimIntData x in extraRestartInt)
                {
                    anim.SetInteger(x.fieldName, x.val);
                }
            }
        }

    }

    [System.Serializable]
    public class AnimIntData
    {
        public string fieldName;
        public int val;
    }

    [System.Serializable]
    public class AnimBoolData
    {
        public string fieldName;
        public bool val;
    }

}
