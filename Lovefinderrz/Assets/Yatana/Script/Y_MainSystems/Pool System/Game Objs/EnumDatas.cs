﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Y_MainSystems
{
    public enum PuzzlePieceKind
    {
        BaseType, BoosterType, ObstacleType, LayeredType
    }

    public enum BaseType
    {
        Ball, Candle, Candy, Clover, Cup, Hat
    }

    public enum BoosterType
    {
        Bomb, Plane, FireworkHorizantal, FireworkVerticle, Rainbow
    }

    public enum ObstacleType
    {
        Box, Barrier
    }

    public enum LayerType
    {
        Ice, Foam
    }

}
