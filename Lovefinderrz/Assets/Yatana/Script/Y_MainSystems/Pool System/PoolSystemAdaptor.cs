﻿using UnityEngine;

namespace Y_MainSystems
{
    public class PoolSystemAdaptor : MonoBehaviour
    {
        [SerializeField]
        GameObject[] objSamples;
        public void Initialize()
        {
            PoolSystem.initilaze(objSamples);
        }
    }
}
