﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Y_MainSystems
{
    public class SoundManeger
    {
       static SoundManeger manager;
       static List<GameAudio> gameAudios;

        static AudioSource musicApollo;
        static AudioSource soundApollo;

        //
        static bool musicOn, soundOn;
        static float musicVolume, soundVolume;
        //

        private SoundManeger()
        {

        }

        public static SoundManeger GetInstance()
        {
            return manager;
        }

        public static void initilaze(GameAudio[] audios, AudioSource music, AudioSource sound)
        {
            manager = new SoundManeger();
            gameAudios = new List<GameAudio>();

            musicApollo = music;
            soundApollo = sound;

            foreach (GameAudio x in audios)
            {
                gameAudios.Add(x);
            }

            musicOn = PlayerPrefs.GetInt("musicOn") == 1;
            soundOn = PlayerPrefs.GetInt("soundOn") == 1;

            musicVolume = PlayerPrefs.GetFloat("musicVolume");
            soundVolume = PlayerPrefs.GetFloat("soundVolume");

        }

        public void PlayMusic(string musicName)
        {
            if (musicOn)
            {
                foreach (GameAudio x in gameAudios)
                {
                    if (x.audioName == musicName)
                    {
                        musicApollo.clip = x.clip;
                        musicApollo.loop = true;
                        musicApollo.volume = musicVolume;
                        musicApollo.Play();
                    }
                }
            }
        }

        public void StopMusic()
        {
            musicApollo.Stop();
        }

        public void PlaySound(string soundName)
        {
            if (soundOn)
            {
                foreach (GameAudio x in gameAudios)
                {
                    if (x.audioName == soundName)
                    {
                        soundApollo.clip = x.clip;
                        soundApollo.volume = soundVolume;
                        soundApollo.Play();
                    }
                }
            }
        }

        public void MusicOn()
        {
            musicOn = true;
            PlayerPrefs.SetInt("musicOn", 1);
            musicApollo.Play();
        }

        public void MusicOff()
        {
            musicOn = false;
            PlayerPrefs.SetInt("musicOn", 0);
            musicApollo.Stop();
        }

        public void SoundOn()
        {
            PlayerPrefs.SetInt("soundOn", 1);
            soundOn = true;
        }

        public void SoundOff()
        {
            PlayerPrefs.SetInt("soundOn", 0);
            soundOn = false;
        }

        public void SetMusicVolume(float volume)
        {
            musicVolume = volume;
            PlayerPrefs.SetFloat("musicVolume", volume);
            musicApollo.volume = volume;
        }

        public void SetSoundValume(float volume)
        {
            soundVolume = volume;
            PlayerPrefs.SetFloat("soundVolume", volume);
            soundApollo.volume = volume;
        }


    }
}
