﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Y_MainSystems
{
    public class GameAudio : MonoBehaviour
    {
        public string audioName;
        public AudioClip clip;
    }
}
