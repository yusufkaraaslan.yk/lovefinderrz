﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Y_MainSystems;

public class SoundManegerAdaptor : MonoBehaviour
{
    public AudioSource musicApollo, soundApollo;
    public GameAudio[] gameAudios;

    private void Awake()
    {
        SoundManeger.initilaze(gameAudios, musicApollo, soundApollo);
    }
}
