﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Y_MainSystems
{
    public class CamSystem
    {
        private static CamSystem camSystem;
        private static List<GameCam> cams;

        public static void initilaze(GameObject[] samples)
        {
            camSystem = new CamSystem();
            cams = new List<GameCam>();

            for (int i = 0; i < samples.Length; i++)
            {
                cams.Add(samples[i].GetComponent<GameCam>());
                cams[i].SetICam(new OffCam(), null);
            }

        }

        public static CamSystem GetInstance()
        {
            return camSystem;
        }

        public void SetCam(string camName, ICam atr, Layout layout)
        {
            for (int i = 0; i < cams.Count; i++)
            {
                if(cams[i].camName == camName)
                {
                    cams[i].SetICam(atr, layout);
                    break;
                }
            }
        }

        public GameCam GetCam(string camName)
        {
            foreach (GameCam x in cams)
            {
                if (x.camName == camName)
                {
                    return x;
                }
            }

            return null;
        }

        private CamSystem()
        {

        }

    }
}
