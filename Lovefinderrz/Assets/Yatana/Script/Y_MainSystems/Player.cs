﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Y_MainSystems;

public class Player : MonoBehaviour
{
    static bool isPlayerOn;

    Vector3 startPos;
    Quaternion startRot;

    [SerializeField] float moveSpeed;
    [SerializeField] GameManeger gm;
    [SerializeField] Animator anim;

    [SerializeField] GameObject playerObject;

    [SerializeField] AudioSource walkSound;

    public static void OpenPlayer()
    {
        isPlayerOn = true;
    }

    public static void ClosePlayer()
    {
        isPlayerOn = false;
    }

    public void Initilaze()
    {
        startPos = transform.position;
        startRot = transform.rotation;
    }

    public void StartPlayer()
    {
        isPlayerOn = true;
        ResetPlayer();
    }

    public void ResetPlayer()
    {
        transform.position = startPos;
        transform.rotation = startRot;
    }

    public void MovePlayer()
    {
        Vector3 way = Vector3.zero;
        bool isPressed = false;

        if (Input.GetKey("w"))
        {
            way = way + Vector3.forward;
            isPressed = true;
        }
        else if (Input.GetKey("s"))
        {
            way = way - Vector3.forward;
            isPressed = true;
        }


        if (Input.GetKey("a"))
        {
            way = way - Vector3.right;
            isPressed = true;
        }
        else if (Input.GetKey("d"))
        {
            way = way + Vector3.right;
            isPressed = true;
        }

        if (isPressed)
        {
            way = way.normalized;

            playerObject.transform.forward = way;
            transform.Translate(way * moveSpeed * Time.deltaTime);

            if (!walkSound.isPlaying)
            {
                //anim.SetBool("IsWalk", true);
                anim.Play("Walk");
                walkSound.Play();
            }
        }
        else
        {
            if (walkSound.isPlaying)
            {
                //anim.SetBool("IsWalk", false);
                anim.Play("Idle");
                walkSound.Stop();
            }
        }

    }

    public void InterectToTarget()
    {
        Time.timeScale = 0.3f;
        UIManeger.GetInstance().OpenUI("Option");
    }

    private void Update()
    {
        if (GameManeger.state == GameState.InGame && isPlayerOn)
        {
            MovePlayer();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "City" && GameManeger.state == GameState.InGame)
        {
            UIManeger.GetInstance().CloseUI("NoSignal");
            SoundManeger.GetInstance().PlayMusic("Crowds");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "City" && GameManeger.state == GameState.InGame)
        {
            UIManeger.GetInstance().OpenUI("NoSignal");
            SoundManeger.GetInstance().PlayMusic("NoSignal");
        }
    }

}
