﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Y_MainSystems
{
    public class UIManeger : MonoBehaviour
    {
        static List<UISetup> uiSetups;
        static UIManeger maneger;

        public static void initilaze(GameObject[] objs)
        {
            maneger = new UIManeger();
            uiSetups = new List<UISetup>();

            for (int i = 0; i < objs.Length; i++)
            {
                uiSetups.Add(objs[i].GetComponent<UISetup>());
            }

            maneger.ClearScreen();
        }

        public static UIManeger GetInstance()
        {
            return maneger;
        }

        public void OpenUI(string setup)
        {
            for (int i = 0; i < uiSetups.Count; i++)
            {
                if(string.Compare(uiSetups[i].UIName, setup) == 0)
                {
                    uiSetups[i].OpenUI();
                    return;
                }
            }

            Debug.LogError(setup + " cant find.");
        }

        public void OpenUIClean(string setup)
        {
            ClearScreen();
            OpenUI(setup);
        }

        public void CloseUI(string setup)
        {
            for (int i = 0; i < uiSetups.Count; i++)
            {
                if (uiSetups[i].UIName == setup)
                {
                    uiSetups[i].CloseUI();
                    return;
                }
            }

            Debug.LogError(setup + " cant find.");
        }

        public void ClearScreen()
        {
            for (int i = 0; i < uiSetups.Count; i++)
            {
                uiSetups[i].CloseUI();
            }
        }

        private UIManeger()
        {

        }
    }
}
