﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Y_MainSystems
{
    public abstract class HelperBase
    {
        public abstract void Work();
        public abstract bool IsComplete();
    }
}
