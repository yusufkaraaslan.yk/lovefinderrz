﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Y_MainSystems;

[CreateAssetMenu(fileName = "Visual Event Profile", menuName = "Visual Event Profile", order = 51)]
public class VisualEventProfile : ScriptableObject
{
    [SerializeField]
    private List<MoveData> moveData;
    [SerializeField]
    private List<AnimData> animData;

    public List<MoveData> MoveData { get => moveData; set => moveData = value; }
    public List<AnimData> AnimData { get => animData; set => animData = value; }
}


