﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Y_MainSystems
{
    public class VisualEventController : MonoBehaviour
    {
        #region Singleton
        private static VisualEventController _instance;

        public static VisualEventController Instance()
        {
            if (_instance == null)
            {
                _instance = new VisualEventController();
            }
            return _instance;
        }

        private VisualEventController()
        {

        }
        #endregion

        public static List<VisualEvent> visualEvents;

        public VisualEvent AddVisualEvent(List<VisualEventData> data)
        {
            VisualEvent visualEvent = new VisualEvent(data);
            visualEvents.Add(visualEvent);
            return visualEvent;
        }

        public void ClearAllEvents()
        {
            visualEvents.Clear();
        }

        public  bool AllEventDone()
        {
            return visualEvents.Count == 0;
        }

        private void Start()
        {
            visualEvents = new List<VisualEvent>();
        }

        private void Update()
        {
            for (int i = visualEvents.Count - 1; i >= 0; i--)
            {
                visualEvents[i].work();

                if (visualEvents[i].IsComplete())
                {
                    visualEvents.Remove(visualEvents[i]);
                }
            }
        }
    }
}

