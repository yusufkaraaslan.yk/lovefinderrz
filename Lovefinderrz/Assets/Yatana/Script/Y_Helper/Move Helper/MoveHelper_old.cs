﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Y_MainSystems
{
    public class MoveHelper_old_Remove : MonoBehaviour
    {
        bool isOn = false;
        bool isAnimComplete;
        bool animLock = false;

        [SerializeField]
        bool isRotate;

        [SerializeField]
        float moveTime;

        [SerializeField]
        InterpType interpType;
        [SerializeField]
        RouteType routeType;


        //float moveTime;
        float currTime;
        GameObject obj;
        Vector3 target;
        Vector3 startPos;
        Queue<Vector3> targetQ;

        //InterpType interpType;
        //RouteType routeType;

        Action callBack;
        Action extra_a;
        Action<Vector2> extra_b;
        Vector2 index;
        Action extra_c;

        private void Start()
        {
            obj = gameObject;
        }

        public bool IsWorking()
        {
            return isOn;
        }

        public void StartMove(Vector3 target, Vector2 ind, Action callBack = null, Action<Vector2> extra = null)
        {
            this.extra_b = extra;
            index = ind;

            initMove(target, callBack);
        }

        public void StartMove(Vector3 target, Action callBack  = null, Action extra = null)
        {
            this.extra_a = extra;

            initMove(target, callBack);
        }

        public void StartMove(Vector3 target, Action extra)
        {
            this.extra_c = extra;

            initMove(target, callBack);
        }

        public void StartMove(Queue<Vector3> target, Action extra)
        {
            this.extra_c = extra;
            targetQ = target;

            initMove(target.Dequeue(), callBack);
        }

        //  For Later use
        public void AddMove(Queue<Vector3> target)
        {
            if (targetQ == null)
            {
                targetQ = new Queue<Vector3>();
            }

            while (target.Count > 0)
            {
                targetQ.Enqueue(target.Dequeue());
            }

            isOn = true;
        }

        public void StopMove()
        {
            isOn = false;

            target = new Vector3();
            targetQ = new Queue<Vector3>();

            ClearExtras();
        }

        public void LockAnimLock()
        {
            animLock = true;
        }

        public void UnlockAnimLock()
        {
            animLock = false;
            isAnimComplete = false;
        }

        public void ExitAnimComplete()
        {
            isAnimComplete = true;
        }

        void initMove(Vector3 target, Action callBack = null)
        {
            this.target = target;

            this.callBack = callBack;

            CalculateMove();

            isOn = true;
            //PuzzlePieceParent.DelayInput();
        }

        void CalculateMove()
        {
            currTime = 0;

            if (routeType == RouteType.Spiral)
            {
                //transform.eulerAngles = new Vector3(0, 0, 45);
                startPos = transform.position;
            }
        }

        void Move()
        {
            Vector3 currPos = obj.transform.position;
            currTime += Time.deltaTime;

            float t = Mathf.Clamp(currTime / moveTime, 0f, 1f);

            switch (interpType)
            {
                case InterpType.Linear:
                    break;
                case InterpType.EaseOut:
                    t = Mathf.Sin(t * Mathf.PI * 0.5f);
                    break;
                case InterpType.EaseIn:
                    t = 1 - Mathf.Cos(t * Mathf.PI * 0.5f);
                    break;
                case InterpType.SmoothStep:
                    t = t * t * (3 - 2 * t);
                    break;
                case InterpType.SmootherStep:
                    t = t * t * t * (t * (t * 6 - 15) + 10);
                    break;
                default:
                    break;
            }

            switch (routeType)
            {
                case RouteType.Lineer:
                    transform.position = Vector3.Lerp(currPos, target, t);
                    break;

                case RouteType.Spiral:
                    Vector3 r = Vector3.Lerp(currPos, target, t) - startPos;
                    float rot = Mathf.Lerp(180, 360, t) * Mathf.PI / 180;

                    if (startPos.x < target.x)
                    {
                        rot = Mathf.Lerp(180, 0, t) * Mathf.PI / 180;
                    }
                    else
                    {
                        rot = Mathf.Lerp(180, 360, t) * Mathf.PI / -180;
                    }

                    Vector3 tar = startPos + new Vector3(r.x * Mathf.Cos(rot) - r.y * Mathf.Sin(rot),
                        r.x * Mathf.Sin(rot) + r.y * Mathf.Cos(rot),
                        0);

                    transform.position = tar;


                    t = Mathf.Clamp((currTime % (moveTime / 2)) / (moveTime / 2), 0f, 1f);

                    if (isRotate)
                    {
                        if (target.x > startPos.x)
                        {
                            if (target.y > startPos.y)
                            {
                                if (currTime < moveTime / 2)
                                {
                                    transform.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(0, -45, t));
                                    //transform.localScale = Vector3.one * Mathf.Lerp(1, 1.8f, t);
                                }
                                else
                                {
                                    transform.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(-45, 45, t));
                                    //transform.localScale = Vector3.one * Mathf.Lerp(1.8f, 0.7f, t);
                                }
                            }
                            else
                            {
                                if (currTime < moveTime / 2)
                                {
                                    transform.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(0, -45, t));
                                    //transform.localScale = Vector3.one * Mathf.Lerp(1, 1.8f, t);
                                }
                                else
                                {
                                    transform.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(-45, -135, t));
                                    //transform.localScale = Vector3.one * Mathf.Lerp(1.8f, 0.7f, t);
                                }
                            }
                        }
                        else
                        {
                            if (target.y > startPos.y)
                            {
                                if (currTime < moveTime / 2)
                                {
                                    transform.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(0, 135, t));
                                    //transform.localScale = Vector3.one * Mathf.Lerp(1, 1.8f, t);
                                }
                                else
                                {
                                    transform.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(135, 45, t));
                                    //transform.localScale = Vector3.one * Mathf.Lerp(1.8f, 0.7f, t);
                                }
                            }
                            else
                            {
                                if (currTime < moveTime / 2)
                                {
                                    transform.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(0, 135, t));
                                    //transform.localScale = Vector3.one * Mathf.Lerp(1, 1.8f, t);
                                }
                                else
                                {
                                    transform.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(135, 225, t));
                                    //transform.localScale = Vector3.one * Mathf.Lerp(1.8f, 0.7f, t);
                                }
                            }
                        }

                    }
                    break;

                default:
                    break;
            }

        }

        void CompleteMove()
        {

            Action callBack = this.callBack;
            Action extra_a = this.extra_a;
            Action<Vector2> extra_b = this.extra_b;
            Vector2 index = this.index;
            Action extra_c = this.extra_c;

            ClearExtras();

            if (extra_a != null)
            {
                extra_a();
            }

            if (extra_b != null)
            {
                extra_b(index);
            }

            if (extra_c != null)
            {
                extra_c();
            }

            if (callBack != null)
            {
                callBack();
            }
        }

        void ClearExtras()
        {
            callBack = null;
            extra_a = null;
            extra_b = null;
            extra_c = null;
        }

        void Update()
        {
            if (isOn)
            {
                if (Vector3.Distance(obj.transform.position, target) < 0.1f || currTime >= moveTime)
                {
                    if (targetQ != null && targetQ.Count > 0)
                    {
                        initMove(targetQ.Dequeue(), callBack);
                    }
                    else
                    {
                        if (animLock)
                        {
                            if (isAnimComplete)
                            {
                                obj.transform.position = target;

                                isOn = false;
                                targetQ = new Queue<Vector3>();
                                animLock = false;
                                CompleteMove();
                            }
                        }
                        else
                        {
                            obj.transform.position = target;

                            isOn = false;
                            targetQ = new Queue<Vector3>();
                            CompleteMove();
                        }
                    }
                }
                else
                {
                    Move();
                }
            }
        }
    }

}
